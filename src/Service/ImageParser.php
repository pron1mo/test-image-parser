<?php

namespace App\Service;

use App\Model\Image;
use App\Model\ImageList;

class ImageParser
{
    public function __construct(
        private readonly HtmlParser $htmlParser
    )
    {
    }

    public function parseImages(string $url): ?ImageList
    {
        $imageLinks = $this->htmlParser->parse($url);
        if (count($imageLinks) === 0) {
            return null;
        }

        $imageList = new ImageList();
        foreach ($imageLinks as $imageLink) {
            $image = $this->processImage($imageLink);
            if ($image instanceof Image) {
                $imageList->addImage($image);
            }
        }

        return $imageList;
    }

    public function processImage(string $url): ?Image
    {
        $headers = get_headers($url, true);
        if ($headers === false || !str_contains($headers['Content-Type'], "image")) return null;

        return new Image($url, $headers['Content-Length']);
    }
}