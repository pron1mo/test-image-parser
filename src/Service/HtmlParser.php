<?php

namespace App\Service;

use Symfony\Component\BrowserKit\HttpBrowser;

class HtmlParser
{
    public function parse(string $url): array
    {
        $parsedUrl = parse_url($url);
        $baseUrl = $parsedUrl['scheme'] . '://' . $parsedUrl['host'];
        $client = new HttpBrowser();

        $crawler = $client->request('GET', $url);
        $nodes = $crawler->filter("img");

        $result = [];
        foreach ($nodes as $node) {
            $src = $node->getAttribute('src');
            if (str_starts_with($src, 'https://mc.yandex.com')) continue;
            if (str_ends_with($src, '.svg')) continue;
            $result[] = $src;
        }

        $result = $this->ensureAbsoluteUrl($baseUrl, $result);

        return $result;
    }

    private function ensureAbsoluteUrl(string $baseUrl, array $urls = []): array
    {
        return array_map(function ($url) use ($baseUrl) {
            if (str_starts_with($url, 'http')) return $url;
            return $baseUrl . $url;
        }, $urls);
    }
}