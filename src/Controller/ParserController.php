<?php

namespace App\Controller;

use App\Service\HtmlParser;
use App\Service\ImageParser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Attribute\Route;

class ParserController extends AbstractController
{
    public const string URL_REGEXP = '/^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/';

    #[Route('/result', name: 'parse_result')]
    public function result(
        #[MapQueryParameter(filter: \FILTER_VALIDATE_REGEXP, options: ['regexp' => self::URL_REGEXP])] string $url,
        ImageParser $imageParser
    ): Response
    {
        $imageList = $imageParser->parseImages($url);

        return $this->render('parser/result.html.twig', [
            'imageList' => $imageList
        ]);
    }
}
