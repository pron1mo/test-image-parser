<?php

namespace App\Model;

class Image
{
    private string $url;
    private int $size;


    public function __construct(string $url, int $size)
    {
        $this->url = $url;
        $this->size = $size;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getSize(): int
    {
        return $this->size;
    }
}