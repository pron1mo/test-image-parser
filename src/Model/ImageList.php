<?php

namespace App\Model;

class ImageList
{
    /** @var Image[] $images  */
    private array $images;

    public function getImages(): array
    {
        return $this->images;
    }

    public function setImages(array $images): self
    {
        $this->images = $images;
        return $this;
    }

    public function addImage(Image $image): self
    {
        $this->images[] = $image;
        return $this;
    }

    public function countImages(): int
    {
        return count($this->images);
    }
    public function totalSize(): int
    {
        return array_reduce($this->images, function (int $total, Image $image) {
            return $total += $image->getSize();
        }, 0);
    }
}