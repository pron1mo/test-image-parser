FROM composer:2 as composer
FROM php:8.3-fpm-alpine as php


RUN set -xe && apk add --no-cache \
    shadow \
    libzip-dev \
    libintl \
    icu \
    icu-dev \
    bash \
    curl \
    libmcrypt \
    libmcrypt-dev \
    libxml2-dev \
    freetype \
    freetype-dev \
    libpng \
    libpng-dev \
    libjpeg-turbo \
    libjpeg-turbo-dev \
    postgresql-dev \
    pcre-dev \
    git \
    g++ \
    make \
    autoconf \
    openssh \
    util-linux-dev \
    libuuid \
    rsync \
    libxslt-dev \
    rabbitmq-c-dev

RUN apk add --update linux-headers

RUN docker-php-ext-configure gd --with-freetype --with-jpeg && \
    docker-php-ext-install -j$(nproc) zip intl pdo_mysql  gd && \
    docker-php-ext-enable sodium

ARG APCU_VERSION=5.1.21
RUN docker-php-ext-install -j$(nproc) \
        soap \
        pcntl \
        exif \
        xsl \
        zip \
        gd \
        intl \
        soap \
        opcache \
        pcntl \
        exif \
        pdo \
        mysqli \
        pdo_mysql && \
#        pecl config-set php_ini $PHP_INI_DIR/php.ini \
    pecl install excimer && \
    pecl install \
        apcu-${APCU_VERSION} && \
    pecl clear-cache && \
    docker-php-ext-enable \
        apcu \
        opcache



RUN mkdir -p /usr/src/php/ext/amqp && \
    curl -fsSL https://pecl.php.net/get/amqp | tar xvz -C "/usr/src/php/ext/amqp" --strip 1 && \
    docker-php-ext-install amqp

RUN pecl install redis && \
    docker-php-ext-enable redis

RUN CFLAGS="$CFLAGS -D_GNU_SOURCE" docker-php-ext-install sockets

RUN echo 'memory_limit=1536M' > /usr/local/etc/php/conf.d/memory.ini \
    && echo 'upload_max_filesize = 10M' >> /usr/local/etc/php/conf.d/upload-size.ini \
    && echo 'post_max_size=20M' >> /usr/local/etc/php/conf.d/upload-size.ini
#    && echo 'session.save_handler=redis' > /usr/local/etc/php/conf.d/session.ini \
#    && echo "session.save_path='tcp://redis:6379?weight=1&read_timeout=2.5'" >> /usr/local/etc/php/conf.d/session.ini


COPY --from=composer /usr/bin/composer /usr/local/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER=1

FROM php AS prod
COPY docker/php/conf.d/symfony.prod.ini /usr/local/etc/php/conf.d/symfony.prod.ini

RUN mkdir -p /var/www/app/vendor && mkdir -p /var/www/app/var/cache

WORKDIR /var/www/app


FROM php AS testing

RUN pecl install pcov && \
    docker-php-ext-enable pcov

# install XDebug extension
RUN apk add --no-cache \
    autoconf \
    $PHPIZE_DEPS \
# TODO: change xdebug version to stable when it's would be available
    && pecl install xdebug-3.3.0alpha3 \
    && docker-php-ext-enable xdebug \
    && apk del --no-cache \
        $PHPIZE_DEPS

FROM testing AS dev

COPY docker/php/conf.d/symfony.dev.ini /usr/local/etc/php/conf.d/symfony.dev.ini

ARG UID
ARG GID
ENV TARGET_UID ${UID:-1000}
ENV TARGET_GID ${GID:-1000}

RUN apk add --no-cache ca-certificates
COPY docker/nginx/ssl/ /usr/local/share/ca-certificates/
RUN update-ca-certificates

RUN usermod -u ${TARGET_UID} www-data && groupmod -g ${TARGET_UID} www-data
RUN mkdir -p /var/www/app/vendor && mkdir -p /var/www/app/var/cache && chown -R www-data:www-data /var/www/app

WORKDIR /var/www/app

USER ${TARGET_UID}:${TARGET_GID}

